import { BrowserRouter, Switch, Route } from 'react-router-dom'
import React from 'react'
import ReactDOM from 'react-dom'
import react, { useState, useEffect, setState }  from 'react';
/* Pages */
import Header from './Components/Header'
import Sidebar from './Components/Sidebar'
import Login from './Pages/Login'
//collection
import CollectionBatch from './Pages/Collection/CollectionBatch'
import CollectionAccounts from './Pages/Collection/CollectionAccounts'
import CollectionDailyReport from './Pages/Collection/CollectionDailyReport'

//litigation
import LitigationAdvocates from './Pages/Litigation/LitigationAdvocates'
import LitigationBatch from './Pages/Litigation/LitigationBatch'
import LitigationCalender from './Pages/Litigation/LitigationCalender'
import LitigationCase from './Pages/Litigation/LitigationCase'

//pre-lirigation
import PreLitigationBatch from './Pages/Pre-Litigation/PreLitigationBatch'
import PreLitigationFir from './Pages/Pre-Litigation/PreLitigationFir'
import PreLitigationNotices from './Pages/Pre-Litigation/PreLitigationNotices'

//others
import Account from './Pages/Account'
import Communication from './Pages/Communication'
import Invoices from './Pages/Invoices'
import Settings from './Pages/Settings'
import SuperAdmin from './Pages/SuperAdmin'
import Users from './Pages/Users'

//charts
import BarRadialChart from './Components/Charts/BarRadialChart'
import CircularChart from './Components/Charts/CircularChart'
import VerticalBarChart from './Components/Charts/VerticalBarChart'
import CircularWithBarChart from './Components/Charts/CircularWithBarChart'
import HorizontalBarChart from './Components/Charts/HorizontalBarChart'
import LineChart from './Components/Charts/LineChart'
import MultiBarCircularChart from './Components/Charts/MultiBarCircularChart'
import StackedHorizontalBarChart from './Components/Charts/StackedHorizontalBarChart'
import MultiCircularMixedChart from './Components/Charts/MultiCircularMixedChart'
import BarChart from './Components/Charts/BarChart'
import BubbleChart from './Components/Charts/BubbleChart'


/* Components */



function App() {


  let verticalBarChartData = {
    titleLeft: "New Calls",
    // titleRight: "Follow-up Calls",
    data: [{
      label: "Connected",
      top: {
        value: 50,
        label: '5,000',
        maxValue: 120,
        color: "#009ec0"
      },
      bottom: {
        value: 90,
        label: '6.0 L',
        maxValue: 120,
        color: "#1de9b6",
        color1: "#1dc4e9"
      }
    },
    {
      label: "PTP",
      top: {
        value: 100,
        label: '2,000',
        maxValue: 100,
        color: "#009ec0"
      },
      bottom: {
        value: 50,
        label: '16.0 L',
        maxValue: 100,
        color: "#1de9b6",
        color1: "#1dc4e9"
      }
    }]
  }

  const circularChartData = {
    data: {
      name: 'Connected',
      value: 23,
      color: "#359aba",
    },
    title: "12, 324",
    tooltipText: "Connected Call"
  }

  const circularWithBarChartData = {
    data: {
      name: 'Connected',
      value: 75,
      color: "#00777c",
      title: "5,266",
    },
    tooltipText: "Connected Call"
  }

  let horizontalChartData = [
    { value: 100, color: "#1dc4e9", color1: "#1de9b6", label: "Not Filled", tooltipLabel: "Cases " },
    { value: 90, color: "#1dc4e9", color1: "#1de9b6", label: "First Hearing", tooltipLabel: "Cases " },
    { value: 60, color: "#1dc4e9", color1: "#1de9b6", label: "Closed", tooltipLabel: "Cases " },
    { value: 60, color: "#1dc4e9", color1: "#1de9b6", label: "Closed", tooltipLabel: "Cases " }
  ];


  let lineChartData = {
    data: [{
      name: 'Jun 12',
      value: 40.7
    }, {
      name: 'Jun 13',
      value: 20.8
    }, {
      name: 'Jun 14',
      value: 96.4
    }, {
      name: 'Jun 15',
      value: 40.8
    }, {
      name: 'Jun 16',
      value: 30.8
    }, {
      name: 'Jun 17',
      value: 48.8
    }, {
      name: 'Jun 18',
      value: 48.8
    }, {
      name: 'Jun 19',
      value: 48.8
    }
    ],
    tooltipLabel: 'Total Collections ',
    width: 800,
    heigth: 300,
    padding: {
      left: 40,
      right: 40,
      top: 20,
      bottom: 20
    }
  }

  let multiBarCircularChartData = {
    data: [
      { name: 'Calls-to-Contacts', value: 75, color: "#359aba" },
      { name: 'Contacts-to-PTP', value: 90, color: "#4fbcce" },
      { name: 'Paid', value: 40, color: "#9debdc" },
    ],
    title: "12, 324",
    subTitle: "Value",

    tooltipText: "Connected Call"
  }

  let stackedHorizontalChartData = [
    {
      rowData: [
        {
          tooltipStartText: "90-180 Days",
          tooltipEndText: "Cr",
          value: 1.2,
          color: "#b2f1e4"
        },
        {
          tooltipStartText: "90-180 Days",
          tooltipEndText: "Cr",
          value: 2.2,
          color: "#6bd0bd"
        },
        {
          tooltipStartText: "90-180 Days",
          tooltipEndText: "Cr",
          value: 4.2,
          color: "#1e897f"
        },
        {
          tooltipStartText: "90-180 Days",
          tooltipEndText: "Cr",
          value: 8.2,
          color: "#4fbdce"
        },
        {
          tooltipStartText: "90-180 Days",
          tooltipEndText: "Cr",
          value: 16.2,
          color: "#b2f1e4"
        },
      ],
      color: "#1dc4e9", color1: "#1de9b6",
      tooltipLabel: "1 ",
      rowName: "DPD Break-up"
    },
    {
      rowData: [
        {
          tooltipStartText: "90-180 Days",
          tooltipEndText: "Cr",
          value: 1.2,
          color: "#b2f1e4"
        },
        {
          tooltipStartText: "90-180 Days",
          tooltipEndText: "Cr",
          value: 1.2,
          color: "#6bd0bd"
        },
        {
          tooltipStartText: "90-180 Days",
          tooltipEndText: "Cr",
          value: 1.2,
          color: "#1e897f"
        },
        {
          tooltipStartText: "90-180 Days",
          tooltipEndText: "Cr",
          value: 1.2,
          color: "#4fbdce"
        },
        {
          tooltipStartText: "90-180 Days",
          tooltipEndText: "Cr",
          value: 1.2,
          color: "#b2f1e4"
        },
      ],
      color: "#1dc4e9", color1: "#1de9b6",
      tooltipLabel: "2 ",
      rowName: "Bre"
    }
  ];


  const multiCircularMixedChartData = {
    data: {
      name: 'Connected',
      value: 75,
      color: "#00777c",
      title: "5,266",
    },
    tooltipText: "Connected Call",
    pieData: [
      {
        value: 30,
        color: "#b2f1e4",
        labelBottom: "0-30 days"
      },
      {
        value: 5,
        color: "#6bd0bd",
        labelBottom: "60-90 days"
      },
      {
        value: 25,
        color: "#1e897f",
        labelBottom: "90-120 days"
      },
      {
        value: 30,
        color: "#4fbdce",
        labelBottom: "120-150 days"
      },
      {
        value: 20,
        color: "#b2f1e4",
        labelBottom: "30-60 days"
      },
    ]
  }

  let barChartData = {
    color: "#1dc4e9",
    color1: "#1de9b6",
    maxValue: 15,
    data: [
      {
        category: 'Callback',
        value: 12.1
      },
      {
        category: 'Promise to pay',
        value: 9.8
      },
      {
        category: 'Refused to pay',
        value: 10.8
      },
      {
        category: 'Surrender',
        value: 5.1
      },
      {
        category: 'Promise to pay1',
        value: 9.8
      },
      {
        category: 'Refused to pay1',
        value: 12.8
      },
      {
        category: 'Callback2',
        value: 5.1
      },
      {
        category: 'Promise to pay2',
        value: 9.8
      },
      {
        category: 'Refused to pay2',
        value: 12.8
      },
    ]
  }

  let bubbleChartData = {
    data: [
      {
        name: 'High',
        value: 7,
        color: "#aae5f7"
      },
      {
        name: 'Medium',
        value: 3,
        color: "#9be2dc"
      },
      {
        name: 'Low',
        value: 1,
        color: "#d5f6ef"
      }
    ]
  }

  let data = {
    data: [
      { name: 'Connected', value: 75, color: "#359aba" },
      { name: 'PTP', value: 23, color: "#4fbcce" },
      { name: 'PTP1', value: 23, color: "#4fbcce" },
      { name: 'Paid', value: 40, color: "#9debdc" }
    ],
    title: "12, 324",
    tooltipText: "Connected Call"
  }
  const [chartData, setNewData] = useState(data);

  function onButtonClick() {
    
    let data = {
      data: [
        { name: 'Connected 1', value: 25, color: "#359aba" },
        { name: 'PTP 1', value: 23, color: "#4fbcce" }
      ],
      title: "12, 324",
      tooltipText: "Connected Call"
    }
    setNewData({...data})
  }


  return (
    <div className="App">
      <BrowserRouter>
        <Header />
        <main >
          <button onClick={onButtonClick}>Click to change data</button>
          <BarRadialChart data={chartData}></BarRadialChart>
          <div style={{ height: '100px' }}></div>

          <BarChart data={barChartData} width="600"></BarChart>
          <div style={{ height: '30px' }}></div>

          <div style={{ display: 'flex' }}>
          <BubbleChart data={bubbleChartData}></BubbleChart>
          <MultiCircularMixedChart scale={1} data={multiCircularMixedChartData}></MultiCircularMixedChart>
          </div>
          <div style={{ height: '100px' }}></div>
          <div style={{ display: 'flex' }}>
            <MultiBarCircularChart data={multiBarCircularChartData}></MultiBarCircularChart>
            <div style={{ padding: "40px" }}>
              <StackedHorizontalBarChart data={stackedHorizontalChartData} scale={1}></StackedHorizontalBarChart>
            </div>
          </div>
          <div style={{ height: '100px' }}></div>
          <LineChart data={lineChartData}></LineChart>
          <div style={{ height: '100px' }}></div>

          <div style={{ display: 'flex' }}>
            <CircularChart data={circularChartData} scale={1}></CircularChart>
            <VerticalBarChart data={verticalBarChartData}></VerticalBarChart>
          </div>

          <div style={{ display: 'flex' }}>
            <CircularWithBarChart data={circularWithBarChartData}></CircularWithBarChart>
            <HorizontalBarChart data={horizontalChartData}></HorizontalBarChart>
          </div>


          <div style={{ height: '100px' }}></div>

          {/* <Switch>
            <Route exact path={'/'}>
              <CollectionBatch />
            </Route>
            <Route exact path={'/login'}>
              <Login />
            </Route>
            <Route exact path={'/collection/batch'}>
              <CollectionBatch />
            </Route>
            <Route exact path={'/collection/accounts'}>
              <CollectionAccounts />
            </Route>
            <Route exact path={'/collection/dailyreport'}>
              <CollectionDailyReport />
            </Route>
            <Route exact path={'/litigation/advocates'}>
              <LitigationAdvocates />
            </Route>
            <Route exact path={'/litigation/batch'}>
              <LitigationBatch />
            </Route>
            <Route exact path={'/litigation/calender'}>
              <LitigationCalender />
            </Route>
            <Route exact path={'/litigation/case'}>
              <LitigationCase />
            </Route>
            <Route exact path={'/prelitigation/batch'}>
              <PreLitigationBatch />
            </Route>
            <Route exact path={'/prelitigation/fir'}>
              <PreLitigationFir />
            </Route>
            <Route exact path={'/prelitigation/notices'}>
              <PreLitigationNotices />
            </Route>

            <Route exact path={'/account'}>
              <Account />
            </Route>
            <Route exact path={'/communication'}>
              <Communication />
            </Route>
            <Route exact path={'/invoices'}>
              <Invoices />
            </Route>
            <Route exact path={'/settings'}>
              <Settings />
            </Route>
            <Route exact path={'/superadmin'}>
              <SuperAdmin />
            </Route>
            <Route exact path={'/users'}>
              <Users />
            </Route>
          </Switch> */}
        </main>
      </BrowserRouter>
    </div>
  )
}

export default App
