import React from 'react'
import { MdSort, MdSearch, MdNotificationsNone } from 'react-icons/md'
import { FaUserCircle } from 'react-icons/fa'
import { useRouteMatch, Link } from 'react-router-dom'
import Logo from '../Static/RawImages/logo.jpeg'

function Header() {

    let urlPointer
    useRouteMatch('/collection/batch') && (urlPointer = 'Batch')
    useRouteMatch('/collection/accounts') && (urlPointer = 'Accounts')


    return (
        <header>
            <img src={Logo} alt="logo" />
            <div className="header">
                <div className="header_left">
                    <MdSort />
                    {urlPointer ?
                        <>
                            <h1> {urlPointer} </h1>
                            <p>Total Result : 1000</p>
                        </>
                        :
                        <h1> Header</h1>
                    }

                </div>
                <div className="header_right">
                    <MdSearch />
                    <MdNotificationsNone />
                    <Link to='/login'>
                        <FaUserCircle />
                    </Link>

                </div>

            </div>

        </header>
    )
}

export default Header
